// @SOURCE:D:/PPlay/se-asg01/conf/routes
// @HASH:e0cc61b5c25054d2750ff0b15200f97152795f77
// @DATE:Sat Mar 14 15:17:14 PDT 2015


import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString

object Routes extends Router.Routes {

import ReverseRouteContext.empty

private var _prefix = "/"

def setPrefix(prefix: String): Unit = {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_index0_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_Application_index0_invoker = createInvoker(
controllers.Application.index(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
        

// @LINE:9
private[this] lazy val controllers_Assets_at1_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_at1_invoker = createInvoker(
controllers.Assets.at(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
        

// @LINE:10
private[this] lazy val controllers_Application_hello2_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("hello"))))
private[this] lazy val controllers_Application_hello2_invoker = createInvoker(
controllers.Application.hello(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "hello", Seq(classOf[String]),"GET", """""", Routes.prefix + """hello"""))
        

// @LINE:11
private[this] lazy val controllers_Products_list3_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("products/"))))
private[this] lazy val controllers_Products_list3_invoker = createInvoker(
controllers.Products.list(fakeValue[Integer]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Products", "list", Seq(classOf[Integer]),"GET", """""", Routes.prefix + """products/"""))
        

// @LINE:12
private[this] lazy val controllers_Products_newProduct4_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("products/new"))))
private[this] lazy val controllers_Products_newProduct4_invoker = createInvoker(
controllers.Products.newProduct(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Products", "newProduct", Nil,"GET", """""", Routes.prefix + """products/new"""))
        

// @LINE:13
private[this] lazy val controllers_Products_details5_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("products/"),DynamicPart("ean", """[^/]+""",true))))
private[this] lazy val controllers_Products_details5_invoker = createInvoker(
controllers.Products.details(fakeValue[models.Product]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Products", "details", Seq(classOf[models.Product]),"GET", """""", Routes.prefix + """products/$ean<[^/]+>"""))
        

// @LINE:14
private[this] lazy val controllers_Products_save6_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("products/"))))
private[this] lazy val controllers_Products_save6_invoker = createInvoker(
controllers.Products.save(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Products", "save", Nil,"POST", """""", Routes.prefix + """products/"""))
        

// @LINE:15
private[this] lazy val controllers_Products_delete7_route = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("products/"),DynamicPart("ean", """[^/]+""",true))))
private[this] lazy val controllers_Products_delete7_invoker = createInvoker(
controllers.Products.delete(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Products", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """products/$ean<[^/]+>"""))
        

// @LINE:16
private[this] lazy val controllers_StockItems_index8_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("stockitems/"))))
private[this] lazy val controllers_StockItems_index8_invoker = createInvoker(
controllers.StockItems.index(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.StockItems", "index", Nil,"GET", """""", Routes.prefix + """stockitems/"""))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """hello""","""controllers.Application.hello(name:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """products/""","""controllers.Products.list(page:Integer ?= 0)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """products/new""","""controllers.Products.newProduct()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """products/$ean<[^/]+>""","""controllers.Products.details(ean:models.Product)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """products/""","""controllers.Products.save()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """products/$ean<[^/]+>""","""controllers.Products.delete(ean:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """stockitems/""","""controllers.StockItems.index()""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]]
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_index0_route(params) => {
   call { 
        controllers_Application_index0_invoker.call(controllers.Application.index())
   }
}
        

// @LINE:9
case controllers_Assets_at1_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at1_invoker.call(controllers.Assets.at(path, file))
   }
}
        

// @LINE:10
case controllers_Application_hello2_route(params) => {
   call(params.fromQuery[String]("name", None)) { (name) =>
        controllers_Application_hello2_invoker.call(controllers.Application.hello(name))
   }
}
        

// @LINE:11
case controllers_Products_list3_route(params) => {
   call(params.fromQuery[Integer]("page", Some(0))) { (page) =>
        controllers_Products_list3_invoker.call(controllers.Products.list(page))
   }
}
        

// @LINE:12
case controllers_Products_newProduct4_route(params) => {
   call { 
        controllers_Products_newProduct4_invoker.call(controllers.Products.newProduct())
   }
}
        

// @LINE:13
case controllers_Products_details5_route(params) => {
   call(params.fromPath[models.Product]("ean", None)) { (ean) =>
        controllers_Products_details5_invoker.call(controllers.Products.details(ean))
   }
}
        

// @LINE:14
case controllers_Products_save6_route(params) => {
   call { 
        controllers_Products_save6_invoker.call(controllers.Products.save())
   }
}
        

// @LINE:15
case controllers_Products_delete7_route(params) => {
   call(params.fromPath[String]("ean", None)) { (ean) =>
        controllers_Products_delete7_invoker.call(controllers.Products.delete(ean))
   }
}
        

// @LINE:16
case controllers_StockItems_index8_route(params) => {
   call { 
        controllers_StockItems_index8_invoker.call(controllers.StockItems.index())
   }
}
        
}

}
     