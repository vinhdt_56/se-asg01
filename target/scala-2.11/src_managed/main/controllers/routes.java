// @SOURCE:D:/PPlay/se-asg01/conf/routes
// @HASH:e0cc61b5c25054d2750ff0b15200f97152795f77
// @DATE:Sat Mar 14 15:17:14 PDT 2015

package controllers;

public class routes {
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseStockItems StockItems = new controllers.ReverseStockItems();
public static final controllers.ReverseProducts Products = new controllers.ReverseProducts();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();

public static class javascript {
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseStockItems StockItems = new controllers.javascript.ReverseStockItems();
public static final controllers.javascript.ReverseProducts Products = new controllers.javascript.ReverseProducts();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
}
          

public static class ref {
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseStockItems StockItems = new controllers.ref.ReverseStockItems();
public static final controllers.ref.ReverseProducts Products = new controllers.ref.ReverseProducts();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
}
          
}
          