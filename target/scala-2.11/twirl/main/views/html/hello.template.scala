
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object hello extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(name:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.15*/("""
"""),format.raw/*2.1*/("""<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Hello</title>
</head>
<body>
<h1>Hello <em>"""),_display_(/*9.16*/name),format.raw/*9.20*/("""</em></h1>
</body>
</html>"""))}
  }

  def render(name:String): play.twirl.api.HtmlFormat.Appendable = apply(name)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (name) => apply(name)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Mar 14 15:17:16 PDT 2015
                  SOURCE: D:/PPlay/se-asg01/app/views/hello.scala.html
                  HASH: d0872cfd1393cd30f5778ed817fc4d655428b43e
                  MATRIX: 723->1|824->14|852->16|997->135|1021->139
                  LINES: 26->1|29->1|30->2|37->9|37->9
                  -- GENERATED --
              */
          