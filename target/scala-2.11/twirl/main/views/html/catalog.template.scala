
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object catalog extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[com.avaje.ebean.Page[Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.Page[Product]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.46*/("""
"""),_display_(/*2.2*/main("Products catalogue")/*2.28*/ {_display_(Seq[Any](format.raw/*2.30*/("""
  """),format.raw/*3.3*/("""<h2>All products</h2>
  <script>
  function del ( urlToDelete ) """),format.raw/*5.32*/("""{"""),format.raw/*5.33*/("""
  """),format.raw/*6.3*/("""$.ajax ( """),format.raw/*6.12*/("""{"""),format.raw/*6.13*/("""
  """),format.raw/*7.3*/("""url : urlToDelete,
  type : 'DELETE',
  success : function ( results ) """),format.raw/*9.34*/("""{"""),format.raw/*9.35*/("""
  """),format.raw/*10.3*/("""// Refresh the page
  location.reload ( ) ;
  """),format.raw/*12.3*/("""}"""),format.raw/*12.4*/("""
  """),format.raw/*13.3*/("""}"""),format.raw/*13.4*/(""" """),format.raw/*13.5*/(""") ;
  """),format.raw/*14.3*/("""}"""),format.raw/*14.4*/("""
  """),format.raw/*15.3*/("""</script>

  <div id="pagination" class="pagination">
    """),_display_(/*18.6*/if(currentPage.hasPrev)/*18.29*/ {_display_(Seq[Any](format.raw/*18.31*/("""
      """),format.raw/*19.7*/("""<li>
        <a href=""""),_display_(/*20.19*/routes/*20.25*/.Products.list(currentPage.getPageIndex - 1)),format.raw/*20.69*/("""">&larr;</a>
      </li>
    """)))}/*22.7*/else/*22.12*/{_display_(Seq[Any](format.raw/*22.13*/("""
      """),format.raw/*23.7*/("""<li class="disabled"><a>&larr;</a></li>
    """)))}),format.raw/*24.6*/("""
    """),format.raw/*25.5*/("""<li class="active"><a>
    """),_display_(/*26.6*/currentPage/*26.17*/.getDisplayXtoYofZ(" - ", " / ")),format.raw/*26.49*/("""</a></li>
    """),_display_(/*27.6*/if(currentPage.hasNext)/*27.29*/ {_display_(Seq[Any](format.raw/*27.31*/("""
      """),format.raw/*28.7*/("""<li>
        <a href=""""),_display_(/*29.19*/routes/*29.25*/.Products.list(currentPage.getPageIndex + 1)),format.raw/*29.69*/("""">&rarr;</a>
      </li>
    """)))}/*31.7*/else/*31.12*/{_display_(Seq[Any](format.raw/*31.13*/("""
      """),format.raw/*32.7*/("""<li class="disabled"><a>&rarr;</a></li>
    """)))}),format.raw/*33.6*/("""
  """),format.raw/*34.3*/("""</div>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>EAN</th>
        <th>Name</th>
        <th>Description</th>
        <th>Date</th>
        <th></th>
      </tr>
    </thead>
    <tbody>    """),_display_(/*46.17*/for(product <- currentPage.getList()) yield /*46.54*/ {_display_(Seq[Any](format.raw/*46.56*/("""
      """),format.raw/*47.7*/("""<tr>
        <td><a href=""""),_display_(/*48.23*/routes/*48.29*/.Products.details(product)),format.raw/*48.55*/("""">
        """),_display_(/*49.10*/product/*49.17*/.ean),format.raw/*49.21*/("""
        """),format.raw/*50.9*/("""</a>
        </td>
        <td><a href=""""),_display_(/*52.23*/routes/*52.29*/.Products.details(product)),format.raw/*52.55*/("""">
        """),_display_(/*53.10*/product/*53.17*/.name),format.raw/*53.22*/("""</a>
        </td>
        <td><a href=""""),_display_(/*55.23*/routes/*55.29*/.Products.details(product)),format.raw/*55.55*/("""">
        """),_display_(/*56.10*/product/*56.17*/.description),format.raw/*56.29*/("""</a>
        </td>
        <td>"""),_display_(/*58.14*/if(product.date != null)/*58.38*/ {_display_(Seq[Any](format.raw/*58.40*/("""
          """),_display_(/*59.12*/product/*59.19*/.date.format("dd-MM-yyyy")),format.raw/*59.45*/("""
        """)))}),format.raw/*60.10*/("""</td>
        <td>
          <a href=""""),_display_(/*62.21*/routes/*62.27*/.Products.details(product)),format.raw/*62.53*/("""">
            <span class="glyphicon glyphicon-pencil"></span></a>
          <a onclick="javascript:del('"""),_display_(/*64.40*/routes/*64.46*/.Products.delete(product.ean)),format.raw/*64.75*/("""')">
            <span class="glyphicon glyphicon-trash"></span></a>
        </td>
      </tr>
    """)))}),format.raw/*68.6*/("""
    """),format.raw/*69.5*/("""</tbody>
  </table>

  <a href=""""),_display_(/*72.13*/routes/*72.19*/.Products.newProduct()),format.raw/*72.41*/("""" class="btn btn-primary">
    New product
  </a>
""")))}))}
  }

  def render(currentPage:com.avaje.ebean.Page[Product]): play.twirl.api.HtmlFormat.Appendable = apply(currentPage)

  def f:((com.avaje.ebean.Page[Product]) => play.twirl.api.HtmlFormat.Appendable) = (currentPage) => apply(currentPage)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Mar 14 15:17:16 PDT 2015
                  SOURCE: D:/PPlay/se-asg01/app/views/catalog.scala.html
                  HASH: d71502443b0eb6d554cb8ae93bfdedbe91b6bb23
                  MATRIX: 748->1|880->45|908->48|942->74|981->76|1011->80|1104->146|1132->147|1162->151|1198->160|1226->161|1256->165|1356->238|1384->239|1415->243|1490->291|1518->292|1549->296|1577->297|1605->298|1639->305|1667->306|1698->310|1786->372|1818->395|1858->397|1893->405|1944->429|1959->435|2024->479|2074->512|2087->517|2126->518|2161->526|2237->572|2270->578|2325->607|2345->618|2398->650|2440->666|2472->689|2512->691|2547->699|2598->723|2613->729|2678->773|2728->806|2741->811|2780->812|2815->820|2891->866|2922->870|3183->1104|3236->1141|3276->1143|3311->1151|3366->1179|3381->1185|3428->1211|3468->1224|3484->1231|3509->1235|3546->1245|3616->1288|3631->1294|3678->1320|3718->1333|3734->1340|3760->1345|3830->1388|3845->1394|3892->1420|3932->1433|3948->1440|3981->1452|4042->1486|4075->1510|4115->1512|4155->1525|4171->1532|4218->1558|4260->1569|4328->1610|4343->1616|4390->1642|4526->1751|4541->1757|4591->1786|4725->1890|4758->1896|4821->1932|4836->1938|4879->1960
                  LINES: 26->1|29->1|30->2|30->2|30->2|31->3|33->5|33->5|34->6|34->6|34->6|35->7|37->9|37->9|38->10|40->12|40->12|41->13|41->13|41->13|42->14|42->14|43->15|46->18|46->18|46->18|47->19|48->20|48->20|48->20|50->22|50->22|50->22|51->23|52->24|53->25|54->26|54->26|54->26|55->27|55->27|55->27|56->28|57->29|57->29|57->29|59->31|59->31|59->31|60->32|61->33|62->34|74->46|74->46|74->46|75->47|76->48|76->48|76->48|77->49|77->49|77->49|78->50|80->52|80->52|80->52|81->53|81->53|81->53|83->55|83->55|83->55|84->56|84->56|84->56|86->58|86->58|86->58|87->59|87->59|87->59|88->60|90->62|90->62|90->62|92->64|92->64|92->64|96->68|97->69|100->72|100->72|100->72
                  -- GENERATED --
              */
          