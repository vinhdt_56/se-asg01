
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String = "Paperclips!")(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.48*/("""
    """),format.raw/*2.5*/("""<!DOCTYPE html>
    <html>
        <head>
            <title>"""),_display_(/*5.21*/title),format.raw/*5.26*/("""</title>
            <script src=""""),_display_(/*6.27*/routes/*6.33*/.Assets.at("javascripts/jquery-2.1.3.js")),format.raw/*6.74*/("""" type="text/javascript"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
            <link href=""""),_display_(/*8.26*/routes/*8.32*/.Assets.at("bootstrap/css/bootstrap.min.css")),format.raw/*8.77*/(""""
            rel="stylesheet" media="screen">
            <link href=""""),_display_(/*10.26*/routes/*10.32*/.Assets.at("stylesheets/main.css")),format.raw/*10.66*/(""""
            rel="stylesheet" media="screen">
            <link rel="shortcut icon" type="image/png"
            href=""""),_display_(/*13.20*/routes/*13.26*/.Assets.at("images/favicon.png")),format.raw/*13.58*/("""">
        </head>
        <body>
            """),_display_(/*16.14*/navigation()),format.raw/*16.26*/("""
            """),format.raw/*17.13*/("""<div class="container">
                """),_display_(/*18.18*/if(flash.containsKey("success"))/*18.50*/ {_display_(Seq[Any](format.raw/*18.52*/("""
                    """),format.raw/*19.21*/("""<div class="alert alert-success">
                    """),_display_(/*20.22*/flash/*20.27*/.get("success")),format.raw/*20.42*/("""
                    """),format.raw/*21.21*/("""</div>
                """)))}),format.raw/*22.18*/("""
                """),_display_(/*23.18*/if(flash.containsKey("error"))/*23.48*/ {_display_(Seq[Any](format.raw/*23.50*/("""
                    """),format.raw/*24.21*/("""<div class="alert alert-error">
                    """),_display_(/*25.22*/flash/*25.27*/.get("error")),format.raw/*25.40*/("""
                    """),format.raw/*26.21*/("""</div>
                """)))}),format.raw/*27.18*/("""
                """),_display_(/*28.18*/content),format.raw/*28.25*/("""
            """),format.raw/*29.13*/("""</div>

            <nav class="nav navbar-default" style="margin-top:20 px">
                <div class="container">
                    <p>Copyright ©2012 paperclips.example.com</p>
                </div>
            </nav>
        </body>
    </html>"""))}
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Mar 14 15:17:16 PDT 2015
                  SOURCE: D:/PPlay/se-asg01/app/views/main.scala.html
                  HASH: ccd0977b824b1382b7555f5182f416cb65ef1667
                  MATRIX: 727->1|861->47|893->53|984->118|1009->123|1071->159|1085->165|1146->206|1338->372|1352->378|1417->423|1518->497|1533->503|1588->537|1739->661|1754->667|1807->699|1884->749|1917->761|1959->775|2028->817|2069->849|2109->851|2159->873|2242->929|2256->934|2292->949|2342->971|2398->996|2444->1015|2483->1045|2523->1047|2573->1069|2654->1123|2668->1128|2702->1141|2752->1163|2808->1188|2854->1207|2882->1214|2924->1228
                  LINES: 26->1|29->1|30->2|33->5|33->5|34->6|34->6|34->6|36->8|36->8|36->8|38->10|38->10|38->10|41->13|41->13|41->13|44->16|44->16|45->17|46->18|46->18|46->18|47->19|48->20|48->20|48->20|49->21|50->22|51->23|51->23|51->23|52->24|53->25|53->25|53->25|54->26|55->27|56->28|56->28|57->29
                  -- GENERATED --
              */
          