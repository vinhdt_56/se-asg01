
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object navigation extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.4*/("""
"""),format.raw/*2.1*/("""<nav class="navbar navbar-inverse " id="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href=""""),_display_(/*5.38*/routes/*5.44*/.Application.index()),format.raw/*5.64*/("""">
        Product Catalog</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li><a href=""""),_display_(/*10.23*/routes/*10.29*/.Application.index()),format.raw/*10.49*/("""">Home</a></li>
        <li><a href=""""),_display_(/*11.23*/routes/*11.29*/.Products.list()),format.raw/*11.45*/("""">Products</a></li>
        <li><a href="">Contact</a></li>
      </ul>
    </div>
  </div>
</nav>"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Mar 14 15:17:16 PDT 2015
                  SOURCE: D:/PPlay/se-asg01/app/views/navigation.scala.html
                  HASH: 4ca43971b7358710b904038b2910436fdb6477dd
                  MATRIX: 721->1|810->3|838->5|1021->162|1035->168|1075->188|1215->301|1230->307|1271->327|1337->366|1352->372|1389->388
                  LINES: 26->1|29->1|30->2|33->5|33->5|33->5|38->10|38->10|38->10|39->11|39->11|39->11
                  -- GENERATED --
              */
          