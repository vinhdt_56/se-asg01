
package views.html.products

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object details extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(productForm: Form[Product]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._
import helper.twitterBootstrap._

Seq[Any](format.raw/*1.30*/("""

"""),format.raw/*5.1*/("""
"""),_display_(/*6.2*/main("Product form")/*6.22*/ {_display_(Seq[Any](format.raw/*6.24*/("""
"""),format.raw/*7.1*/("""<h1>Product form</h1>
"""),_display_(/*8.2*/helper/*8.8*/.form(action = routes.Products.save())/*8.46*/ {_display_(Seq[Any](format.raw/*8.48*/("""
"""),format.raw/*9.1*/("""<fieldset>
    <input type="hidden" value=""""),_display_(/*10.34*/productForm("id")/*10.51*/.valueOr("")),format.raw/*10.63*/("""" name="id"/>
    <legend>Product ("""),_display_(/*11.23*/productForm("name")/*11.42*/.valueOr("New")),format.raw/*11.57*/(""")</legend>
    """),_display_(/*12.6*/helper/*12.12*/.inputText(productForm("ean"), '_label -> "EAN")),format.raw/*12.60*/("""
    """),_display_(/*13.6*/helper/*13.12*/.inputText(productForm("name"), '_label -> "Name")),format.raw/*13.62*/("""
    """),_display_(/*14.6*/helper/*14.12*/.textarea(productForm("description"), '_label -> "Description")),format.raw/*14.75*/("""

"""),format.raw/*16.1*/("""</fieldset>
    <div class="control-group">
        <div class="controls">
            <input name="tags[0].id" value="1" type="checkbox"
            """),_display_(/*20.14*/for(i <- 0 to 2) yield /*20.30*/ {_display_(Seq[Any](format.raw/*20.32*/("""
            """),_display_(/*21.14*/if(productForm("tags[" + i + "].id").value == "1")/*21.64*/ {_display_(Seq[Any](format.raw/*21.66*/(""" """),format.raw/*21.67*/("""checked """)))}),format.raw/*21.76*/("""
            """)))}),format.raw/*22.14*/(""" """),format.raw/*22.15*/("""> lightweight
            <input name="tags[1].id" value="2" type="checkbox"
            """),_display_(/*24.14*/for(i <- 0 to 2) yield /*24.30*/ {_display_(Seq[Any](format.raw/*24.32*/("""
            """),_display_(/*25.14*/if(productForm("tags[" + i + "].id").value == "2")/*25.64*/ {_display_(Seq[Any](format.raw/*25.66*/(""" """),format.raw/*25.67*/("""checked """)))}),format.raw/*25.76*/("""
            """)))}),format.raw/*26.14*/(""" """),format.raw/*26.15*/("""> metal
            <input name="tags[2].id" value="3" type="checkbox"
                """),_display_(/*28.18*/for(i <- 0 to 2) yield /*28.34*/ {_display_(Seq[Any](format.raw/*28.36*/("""
                    """),_display_(/*29.22*/if(productForm("tags[" + i + "].id").value == "3")/*29.72*/ {_display_(Seq[Any](format.raw/*29.74*/(""" """),format.raw/*29.75*/("""checked """)))}),format.raw/*29.84*/("""
        """)))}),format.raw/*30.10*/(""" """),format.raw/*30.11*/("""> plastic
        </div>
    </div>
<input type="submit" class="btn btn-primary" value="Save">
<a class="btn" href=""""),_display_(/*34.23*/routes/*34.29*/.Products.list()),format.raw/*34.45*/("""">Cancel</a>
""")))}),format.raw/*35.2*/("""
""")))}))}
  }

  def render(productForm:Form[Product]): play.twirl.api.HtmlFormat.Appendable = apply(productForm)

  def f:((Form[Product]) => play.twirl.api.HtmlFormat.Appendable) = (productForm) => apply(productForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Mar 14 15:17:16 PDT 2015
                  SOURCE: D:/PPlay/se-asg01/app/views/products/details.scala.html
                  HASH: ed34379ead2714d7786190a003a5bd6d07dd966e
                  MATRIX: 741->1|905->29|935->86|963->89|991->109|1030->111|1058->113|1107->137|1120->143|1166->181|1205->183|1233->185|1305->230|1331->247|1364->259|1428->296|1456->315|1492->330|1535->347|1550->353|1619->401|1652->408|1667->414|1738->464|1771->471|1786->477|1870->540|1901->544|2083->699|2115->715|2155->717|2197->732|2256->782|2296->784|2325->785|2365->794|2411->809|2440->810|2559->902|2591->918|2631->920|2673->935|2732->985|2772->987|2801->988|2841->997|2887->1012|2916->1013|3033->1103|3065->1119|3105->1121|3155->1144|3214->1194|3254->1196|3283->1197|3323->1206|3365->1217|3394->1218|3542->1339|3557->1345|3594->1361|3639->1376
                  LINES: 26->1|30->1|32->5|33->6|33->6|33->6|34->7|35->8|35->8|35->8|35->8|36->9|37->10|37->10|37->10|38->11|38->11|38->11|39->12|39->12|39->12|40->13|40->13|40->13|41->14|41->14|41->14|43->16|47->20|47->20|47->20|48->21|48->21|48->21|48->21|48->21|49->22|49->22|51->24|51->24|51->24|52->25|52->25|52->25|52->25|52->25|53->26|53->26|55->28|55->28|55->28|56->29|56->29|56->29|56->29|56->29|57->30|57->30|61->34|61->34|61->34|62->35
                  -- GENERATED --
              */
          