
package views.html.products

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object list extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[com.avaje.ebean.Page[Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.Page[Product]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.46*/("""
"""),_display_(/*2.2*/main("Products catalogue")/*2.28*/ {_display_(Seq[Any](format.raw/*2.30*/("""  """),format.raw/*2.32*/("""<h2>All products</h2>
<script>
    function del(urlToDelete) """),format.raw/*4.31*/("""{"""),format.raw/*4.32*/("""
        """),format.raw/*5.9*/("""$.ajax ( """),format.raw/*5.18*/("""{"""),format.raw/*5.19*/("""
            """),format.raw/*6.13*/("""url : urlToDelete,
            type : 'DELETE',
            success : function ( results ) """),format.raw/*8.44*/("""{"""),format.raw/*8.45*/("""
                """),format.raw/*9.17*/("""// Refresh the page
                location.reload ( ) ;
            """),format.raw/*11.13*/("""}"""),format.raw/*11.14*/("""
        """),format.raw/*12.9*/("""}"""),format.raw/*12.10*/(""" """),format.raw/*12.11*/(""") ;
    """),format.raw/*13.5*/("""}"""),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</script>
<div id="pagination" class="pagination">
    """),_display_(/*16.6*/if(currentPage.hasPrev)/*16.29*/ {_display_(Seq[Any](format.raw/*16.31*/("""
    """),format.raw/*17.5*/("""<li>
        <a href=""""),_display_(/*18.19*/routes/*18.25*/.Products.list(currentPage.getPageIndex - 1)),format.raw/*18.69*/("""">
            &larr;
        </a>
    </li>
    """)))}/*22.7*/else/*22.12*/{_display_(Seq[Any](format.raw/*22.13*/("""
    """),format.raw/*23.5*/("""<li class="disabled"><a>&larr;</a></li>
    """)))}),format.raw/*24.6*/("""
    """),format.raw/*25.5*/("""<li class="active">
        <a>"""),_display_(/*26.13*/currentPage/*26.24*/.getDisplayXtoYofZ(" - ", " / ")),format.raw/*26.56*/("""</a>
    </li>
    """),_display_(/*28.6*/if(currentPage.hasNext)/*28.29*/ {_display_(Seq[Any](format.raw/*28.31*/("""
    """),format.raw/*29.5*/("""<li>
        <a href=""""),_display_(/*30.19*/routes/*30.25*/.Products.list(currentPage.getPageIndex + 1)),format.raw/*30.69*/("""">&rarr;</a>
    </li>
    """)))}/*32.7*/else/*32.12*/{_display_(Seq[Any](format.raw/*32.13*/("""
    """),format.raw/*33.5*/("""<li class="disabled"><a>&rarr;</a></li>
    """)))}),format.raw/*34.6*/("""
"""),format.raw/*35.1*/("""</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>EAN</th>
        <th>Name</th>
        <th>Description</th>
        <th>Date</th>
        <th> </th>
    </tr>
    </thead>
    <tbody>
    """),_display_(/*48.6*/for(product <- currentPage.getList()) yield /*48.43*/ {_display_(Seq[Any](format.raw/*48.45*/("""
    """),format.raw/*49.5*/("""<tr>
        <td><a href=""""),_display_(/*50.23*/routes/*50.29*/.Products.details(product)),format.raw/*50.55*/("""">
            """),_display_(/*51.14*/product/*51.21*/.ean),format.raw/*51.25*/("""
        """),format.raw/*52.9*/("""</a></td>
        <td><a href=""""),_display_(/*53.23*/routes/*53.29*/.Products.details(product)),format.raw/*53.55*/("""">
            """),_display_(/*54.14*/product/*54.21*/.name),format.raw/*54.26*/("""</a></td>
        <td><a href=""""),_display_(/*55.23*/routes/*55.29*/.Products.details(product)),format.raw/*55.55*/("""">
            """),_display_(/*56.14*/product/*56.21*/.description),format.raw/*56.33*/("""</a></td>
        <td>"""),_display_(/*57.14*/if(product.date != null)/*57.38*/ {_display_(Seq[Any](format.raw/*57.40*/("""
            """),_display_(/*58.14*/product/*58.21*/.date.format("dd-MM-yyyy")),format.raw/*58.47*/("""
        """)))}),format.raw/*59.10*/("""
        """),format.raw/*60.9*/("""</td>
        <td><a onclick="javascript:del('"""),_display_(/*61.42*/routes/*61.48*/.Products.delete(product.ean)),format.raw/*61.77*/("""')" >
            <span class="glyphicon glyphicon-trash"></span>
        </a></td>
    </tr>
    """)))}),format.raw/*65.6*/("""
    """),format.raw/*66.5*/("""</tbody>
</table>
<a href=""""),_display_(/*68.11*/routes/*68.17*/.Products.newProduct()),format.raw/*68.39*/("""" class="btn btn-primary">New product</a>
""")))}))}
  }

  def render(currentPage:com.avaje.ebean.Page[Product]): play.twirl.api.HtmlFormat.Appendable = apply(currentPage)

  def f:((com.avaje.ebean.Page[Product]) => play.twirl.api.HtmlFormat.Appendable) = (currentPage) => apply(currentPage)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Mar 14 15:17:16 PDT 2015
                  SOURCE: D:/PPlay/se-asg01/app/views/products/list.scala.html
                  HASH: c5837668ceed86eb82225263237a1e6fb7f2cf72
                  MATRIX: 754->1|886->45|914->48|948->74|987->76|1016->78|1106->141|1134->142|1170->152|1206->161|1234->162|1275->176|1395->269|1423->270|1468->288|1568->360|1597->361|1634->371|1663->372|1692->373|1728->382|1756->383|1785->385|1869->443|1901->466|1941->468|1974->474|2025->498|2040->504|2105->548|2177->603|2190->608|2229->609|2262->615|2338->661|2371->667|2431->700|2451->711|2504->743|2552->765|2584->788|2624->790|2657->796|2708->820|2723->826|2788->870|2836->901|2849->906|2888->907|2921->913|2997->959|3026->961|3283->1192|3336->1229|3376->1231|3409->1237|3464->1265|3479->1271|3526->1297|3570->1314|3586->1321|3611->1325|3648->1335|3708->1368|3723->1374|3770->1400|3814->1417|3830->1424|3856->1429|3916->1462|3931->1468|3978->1494|4022->1511|4038->1518|4071->1530|4122->1554|4155->1578|4195->1580|4237->1595|4253->1602|4300->1628|4342->1639|4379->1649|4454->1697|4469->1703|4519->1732|4652->1835|4685->1841|4742->1871|4757->1877|4800->1899
                  LINES: 26->1|29->1|30->2|30->2|30->2|30->2|32->4|32->4|33->5|33->5|33->5|34->6|36->8|36->8|37->9|39->11|39->11|40->12|40->12|40->12|41->13|41->13|42->14|44->16|44->16|44->16|45->17|46->18|46->18|46->18|50->22|50->22|50->22|51->23|52->24|53->25|54->26|54->26|54->26|56->28|56->28|56->28|57->29|58->30|58->30|58->30|60->32|60->32|60->32|61->33|62->34|63->35|76->48|76->48|76->48|77->49|78->50|78->50|78->50|79->51|79->51|79->51|80->52|81->53|81->53|81->53|82->54|82->54|82->54|83->55|83->55|83->55|84->56|84->56|84->56|85->57|85->57|85->57|86->58|86->58|86->58|87->59|88->60|89->61|89->61|89->61|93->65|94->66|96->68|96->68|96->68
                  -- GENERATED --
              */
          