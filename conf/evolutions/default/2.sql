# --- Initalize tags
# --- !Ups
INSERT INTO tag(id,name) values (1, 'lightweight');
INSERT INTO tag(id,name) values (2, 'metal');
INSERT INTO tag(id,name) values (3, 'plastic');

# --- Downs

SET REFERENTLAL_INTEGRITY FALSE ;

DELETE from tag;
