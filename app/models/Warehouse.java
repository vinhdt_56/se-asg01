package models;

import java.util.*;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class Warehouse extends Model{
    @Id
    public Long id;

    public String name;
    @OneToOne
    public Address address;
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();  // trường quan hệ
    public static Finder<Long,Warehouse> find = new Finder<Long,Warehouse>(
            Long.class, Warehouse.class
    );

    public String toString() {
        return name;
    }

}