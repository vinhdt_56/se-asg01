package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.*;

@Entity
public class Tag extends Model {

    @Id
    public Long id;
    public String name;
    @ManyToMany(mappedBy="tags")
    public List<Product> products;

    public static Finder<Long,Tag> find = new Finder<Long,Tag>(
            Long.class, Tag.class
    );
    public static Tag findById(Long id) {
        return find.byId(id);
    }

    public Tag(){
        // Left empty
    }
    public Tag(Long id, String name, Collection<Product> products) {
        this.id = id;
        this.name = name;
        this.products = new LinkedList<Product>(products);
        for (Product product : products) {
            product.tags.add(this);
        }
    }

}